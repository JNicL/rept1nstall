include(CMakeParseArguments)

function(BuildForm)
  cmake_parse_arguments(_form
      ""
      ""
      "CONFIGURE_COMMAND;BUILD_COMMAND;INSTALL_COMMAND"
      ${ARGN})
    if(NOT _form_CONFIGURE_COMMAND AND NOT _form_BUILD_COMMAND
       AND NOT _form_INSTALL_COMMAND)
    message(FATAL_ERROR "Must pass at least one of CONFIGURE_COMMAND,
    BUILD_COMMAND, INSTALL_COMMAND")
    endif()
  ExternalProject_Add(form
    PREFIX ${DEPS_BUILD_DIR}
    URL ${COLLIER_URL}
    DOWNLOAD_DIR ${DEPS_DOWNLOAD_DIR}/form
    DOWNLOAD_COMMAND ${CMAKE_COMMAND}
      -DPREFIX=${DEPS_BUILD_DIR}
      -DDOWNLOAD_DIR=${DEPS_DOWNLOAD_DIR}/form
      -DURL=${FORM_URL}
      -DEXPECTED_SHA256=${FORM_SHA256}
      -DTARGET=form
      -DUSE_EXISTING_SRC_DIR=${USE_EXISTING_SRC_DIR}
      -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/DownloadAndExtractFile.cmake
      BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND "${_form_CONFIGURE_COMMAND}"
    BUILD_COMMAND "${_form_BUILD_COMMAND}"
    INSTALL_COMMAND "${_form_INSTALL_COMMAND}"
    )
endfunction()

set(FORM_SRC_DIR ${DEPS_BUILD_DIR}/src/form)



set(FORM_CONFIGURE_COMMAND
    autoreconf -i &&
    ${DEPS_BUILD_DIR}/src/form/configure --prefix=${DEPS_INSTALL_DIR})

set(FORM_BUILD_COMMAND ${MAKE_PRG})
set(FORM_INSTALL_COMMAND ${MAKE_PRG} install)

BuildForm(CONFIGURE_COMMAND ${FORM_CONFIGURE_COMMAND}
          BUILD_COMMAND ${FORM_BUILD_COMMAND}
          INSTALL_COMMAND ${FORM_INSTALL_COMMAND}
          )

list(APPEND THIRD_PARTY_DEPS form)

include(CMakeParseArguments)


message("ip ${DEPS_INSTALL_DIR}")
function(BuildCollier)
  cmake_parse_arguments(_collier
      ""
      ""
      "CONFIGURE_COMMAND;BUILD_COMMAND"
      ${ARGN})
    if(NOT _collier_CONFIGURE_COMMAND AND NOT _collier_BUILD_COMMAND
       AND NOT _collier_INSTALL_COMMAND)
    message(FATAL_ERROR "Must pass at least one of CONFIGURE_COMMAND,
    BUILD_COMMAND, INSTALL_COMMAND")
    endif()

  ExternalProject_Add(collier
    PREFIX ${DEPS_INSTALL_DIR}
    URL ${COLLIER_URL}
    DOWNLOAD_DIR ${DEPS_DOWNLOAD_DIR}/collier
    DOWNLOAD_COMMAND ${CMAKE_COMMAND}
      -DPREFIX=${DEPS_BUILD_DIR}
      -DDOWNLOAD_DIR=${DEPS_DOWNLOAD_DIR}/collier
      -DURL=${COLLIER_URL}
      -DEXPECTED_SHA256=${COLLIER_SHA256}
      -DTARGET=collier
      -DUSE_EXISTING_SRC_DIR=${USE_EXISTING_SRC_DIR}
      -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/DownloadAndExtractFile.cmake
      BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND "${_collier_CONFIGURE_COMMAND}"
    BUILD_COMMAND "${_collier_BUILD_COMMAND}"
    INSTALL_COMMAND "${_collier_INSTALL_COMMAND}"
    )
endfunction()

set(COLLIER_SRC_DIR ${DEPS_BUILD_DIR}/src/collier)

set(COLLIER_CONFIGURE_COMMAND
  ${CMAKE_COMMAND} ${COLLIER_SRC_DIR} -DCMAKE_INSTALL_PREFIX=${DEPS_INSTALL_DIR})

set(COLLIER_BUILD_COMMAND ${CMAKE_COMMAND} --build .)
set(COLLIER_INSTALL_COMMAND ${CMAKE_COMMAND} --build . --target install)

BuildCollier(CONFIGURE_COMMAND ${COLLIER_CONFIGURE_COMMAND}
             BUILD_COMMAND ${COLLIER_BUILD_COMMAND}
             INSTALL_COMMAND ${COLLIER_INSTALL_COMMAND}
             )

list(APPEND THIRD_PARTY_DEPS collier)
#if(EXISTS "${COLLIER_SRC_DIR}/libcollier.so")
#  install (FILES ${COLLIER_SRC_DIR}/libcollier.so DESTINATION ${DEPS_INSTALL_DIR}/lib)
#  endif()
#if (EXISTS "${COLLIER_SRC_DIR}/libcollier.a")
#  install (FILES ${COLLIER_SRC_DIR}/libcollier.a DESTINATION ${DEPS_INSTALL_DIR}/lib)
#endif()
#if (EXISTS "${COLLIER_SRC_DIR}/libcollier.dylib")
#  install (FILES ${COLLIER_SRC_DIR}/libcollier.dylib DESTINATION ${DEPS_INSTALL_DIR}/lib)
#endif()
#install (FILES ${COLLIER_SRC_DIR}/modules/collier.mod DESTINATION ${DEPS_INSTALL_DIR}/modules)

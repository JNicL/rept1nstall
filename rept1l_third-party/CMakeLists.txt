cmake_minimum_required (VERSION 2.8.12)
project(REPT1L_DEPS)

# Point CMake at any custom modules we may ship
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")
set(DEPS_INSTALL_DIR "${CMAKE_INSTALL_PREFIX}" CACHE PATH "Dependencies install directory.")
set(DEPS_BIN_DIR "${DEPS_INSTALL_DIR}/bin" CACHE PATH "Dependencies binary install directory.")
set(DEPS_LIB_DIR "${DEPS_INSTALL_DIR}/lib" CACHE PATH "Dependencies library install directory.")
set(DEPS_BUILD_DIR "${CMAKE_BINARY_DIR}/build" CACHE PATH "Dependencies build directory.")
set(DEPS_DOWNLOAD_DIR "${DEPS_BUILD_DIR}/downloads" CACHE PATH "Dependencies download directory.")

option(BUILD_ALL "Build all third party dependencies." OFF)
option(BUILD_COLLIER "Build COLLIER." ${BUILD_ALL})
option(BUILD_FORM "Build FORM." ${BUILD_FORM})

option(USE_EXISTING_SRC_DIR "Skip download of deps sources in case of existing source directory." OFF)

if(UNIX)
  find_program(MAKE_PRG NAMES gmake make)
  if(MAKE_PRG)
    execute_process(
      COMMAND "${MAKE_PRG}" --version
      OUTPUT_VARIABLE MAKE_VERSION_INFO)
    if(NOT "${OUTPUT_VARIABLE}" MATCHES ".*GNU.*")
      unset(MAKE_PRG)
    endif()
  endif()

  if(NOT MAKE_PRG)
    message(FATAL_ERROR "GNU Make is required to build the dependencies.")
  else()
    message(STATUS "Found GNU Make at ${MAKE_PRG}")
  endif()
endif()

# When using make, use the $(MAKE) variable to avoid warning about the job
# server.
if(CMAKE_GENERATOR MATCHES "Makefiles")
  set(MAKE_PRG "$(MAKE)")
endif()

if(CMAKE_C_COMPILER_ARG1)
  set(DEPS_C_COMPILER "${CMAKE_C_COMPILER} ${CMAKE_C_COMPILER_ARG1}")
else()
  set(DEPS_C_COMPILER "${CMAKE_C_COMPILER}")
endif()

include(ExternalProject)
#set(COLLIER_URL http://www.hepforge.org/archive/collier/collier-1.0.tar.gz)
#set(COLLIER_SHA256 54f40c1ed07a6829230af400abfe48791e74e56eac2709c0947cec3410a4473d)
#set(COLLIER_URL http://www.hepforge.org/archive/collier/collier-1.2.3.tar.gz)
#set(COLLIER_SHA256 e6f72df223654df59113b0067a4bebe9f8c20227bb81371d3193e1557bdf56fb)
set(COLLIER_URL http://www.hepforge.org/archive/collier/collier-1.2.5.tar.gz)
set(COLLIER_SHA256 3ec58a975ff0c3b1ca870bc38973476c923ff78fd3dd5850e296037852b94a8b)

set(FORM_URL https://github.com/vermaseren/form/archive/v4.1-20131025.tar.gz)
set(FORM_SHA256 caece2c6e605ccf32eb3612c4ed5c9257a7a62824ad219c5e46b6d00066f1ba6)

if(BUILD_COLLIER)
  include(BuildCollier)
endif()

if(BUILD_FORM)
  include(BuildForm)
endif()

add_custom_target(clean-all
  COMMAND ${CMAKE_COMMAND} -P ${PROJECT_SOURCE_DIR}/cmake/CleanAll.cmake
)

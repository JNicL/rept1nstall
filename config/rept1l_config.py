# Set your REPT1L config here, default "REPTIL_MODEL_PATH"
#rept1l_environ = 'rmp'


# Enable ct expansion for derived paramters (expected to yield faster ct
# expansion of couplings)
derived_ct_expansion = True


# check if particle requires mass ct during renormalization
check_massless_renormalization = False


# True by default
#complex_mass_scheme = False


# True by default
#realmomentum = True


# by default 200
#couplings_per_mod = 300


# by default 50
#ctcouplings_per_mod = 100


# by default 800
#vertices_per_mod = 500


# by default False
#symmetrize_currents = False

# by default False
reduce_dalgebra = False

# by default form executable from PATH
#formcmd = 'path/to/form'


# Merge bases, recommended
optimize_base = False

#Restrict the merged base length. Expects NONE (no limit) or positive integer.
#max_base_length = 10


# 0 individual file for each current
# 1 group by lorentz structure
# 2 group by lorentz structure, grouping including permutation
split_form_currents = 0
split_tree_currents = 1
split_loop_currents = 1

# apply common-subexpression simplification on current functions, strongly
# recommended
apply_cse_currents = True


# call a simplification on current functions
simplify_currents = True


# if simplify_currents is enabled, use mathematica instead
mathematica_simplify = False


# use predefined recola base, if possible
recola_base = True

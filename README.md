# REPT1l's Install Script #

* Version 1.0.0

* * *

Automatic install for **REPT1L**. 


## Installation

Start the installation with:

    python install.py

select (1) and follow further instructions.

* * *

### Dependencies

+ Python 2.7
+ CMake  2.8.7 or higher.
+ pip    7.0 or higher

* * *

### Contact ###

* Repo owner: Jean-Nicolas Lang <jlang@physik.uni-wuerzburg.de>

#!/usr/bin/env python==========================================================#
#                                  install.py                                  #
#==============================================================================#
from __future__ import print_function

__doc__ = """Rept1l installation script. """

############
#  Config  #
############

# Options when selecting auto installation is selected
get_rept1l = True  # set to True if recola should be downloaded
rept1l_git = True  # ... from repository if True, else release
rept1l_git_url = 'https://JNicL@bitbucket.org/JNicL/rept1l.git'


get_recola = True  # set to True if recola should be downloaded
recola_git = True  # ... from repository if True, else release
recola_git_url = 'https://JNicL@bitbucket.org/JNicL/recola2.0.git'

# Form version fixed to 4.1, see CMakeLists.txt in rept1l_third-party
# You can force to build form even if it is found in PATH
build_form = True

# Collier version fixed to 1.0, see CMakeLists.txt in rept1l_third-party
build_collier = True
rept1l_pip_requirements = True

#===========#
#  Imports  #
#===========#
import os
import sys
import platform
import subprocess
import readline
import glob
from six.moves.urllib.request import urlretrieve
import tarfile
from six.moves import input

#=====================#
#  Globals & Methods  #
#=====================#

def color(string):
  return '\033[1;32m' + string + '\033[1;m'

RecolaID = '\033[1;31mRECOLA\033[1;m'
Rept1lID = '\033[1;31mREPT1L\033[1;m'
Rept1lC = color('REPT1L')
RecolaC = color('RECOLA')
CollierC = color('COLLIER')
FormC = color('FORM')

coloredsymbols = {'recola': RecolaC, 'rept1l': Rept1lC,
                  'collier': CollierC, 'form': FormC}

# install path relative to this file
install_dir = os.path.dirname(os.path.abspath(__file__))
install_dir_ID = color(install_dir)

# config path
config_dir_relative_path = '.config/rept1l'
config_dir_abosulte_path = os.path.join(os.path.expanduser("~"),
                                        config_dir_relative_path)
config_dir_ID = '\033[1;32m' + config_dir_abosulte_path + '\033[1;m'

se = """ Welcome to the %(rept1l)s installation.

 %(rept1l2)s is installed to:

  %(install_dir)s

 %(rept1l2)s's config will be located at:

  %(config_dir)s

 You can change the installation path by moving the installation script to the
 desired location.
 Please select how to proceed:

 1) Full installation and automatic configuration:
    Installs %(rept1l2)s and all dependencies automatically.

 2) %(rept1l2)s installation and manual configuration:
    %(rept1l2)s is installed, all dependencies need to be installed by hand.
    The whizard setup will query for necessary paths.

 3) %(rept1l2)s installation and no configuartion:
    Only %(rept1l2)s is installed, all dependencies need to be installed and
    configured by hand.

 4) Quit %(rept1l2)s installation

""" % {'rept1l': Rept1lID, 'rept1l2': Rept1lC, 'install_dir': install_dir_ID,
       'config_dir': config_dir_ID}


end = """ Completed %(rept1l)s installation.
""" % {'rept1l': Rept1lID}

SUPPORTED_SHELLS = ('bash', 'zsh')

make_cmd = None
cmake_cmd = None
pip_cmd = None
git_cmd = None

#------------------------------------------------------------------------------#

def query_yes_no(question, default="yes"):
  """ Ask a yes/no question via input() and return their answer.

  :question: string that is presented to the user.

  :default: default accepted answer when hitting <Enter>
  """
  valid = {'yes': True, 'y': True, 'ye': True, 'Y': True, 'no': False,
           'n': False, 'N': False}
  if default is None:
    prompt = " [y/n] "
  elif default == "yes":
    prompt = " [Y/n] "
  elif default == "no":
    prompt = " [y/N] "
  else:
    raise ValueError("invalid default answer: '%s'" % default)

  sys.stdout.write(question + prompt)
  while True:
    choice = input().lower()
    if default is not None and choice == '':
      return valid[default]
    elif choice in valid:
      return valid[choice]
    else:
      sys.stdout.write("Please respond with 'yes' or 'no' "
                       "(or 'y' or 'n').\n" + prompt)

#------------------------------------------------------------------------------#

def query_case(question, cases, default=1):
  """ Ask for a certain case choice via input() and return their answer.

  :question: string that is presented to the user.

  :cases: Number of different choices.

  :default: default accepted answer when hitting <Enter>
  """
  def valid(answer):
    try:
      return int(answer) > 0 and int(answer) <= cases
    except:
      return False

  prompt = " [" + ','.join('(' + str(u) + ')' if u == default else str(u)
                           for u in range(1, cases + 1)) + "] "

  sys.stdout.write(question + prompt)
  while True:
    choice = input().lower()
    if default is not None and choice == '':
      return default
    elif valid(choice):
      return int(choice)
    else:
      sys.stdout.write("Please enter one of the cases: \n" + prompt)

#------------------------------------------------------------------------------#

def get_shell():
  return os.path.basename(os.getenv('SHELL', ''))

#------------------------------------------------------------------------------#

def verify_requirements_essential():
  # check shell
  if not get_shell() in SUPPORTED_SHELLS:
    print("Your SHELL %s is not supported" % get_shell(), file=sys.stderr)
    sys.exit(1)

  # check python
  if sys.version_info[0] == 2 and sys.version_info[1] < 7:
    print("Python v2.7+ required.", file=sys.stderr)
    sys.exit(1)

  # check pip
  p = subprocess.Popen('which pip', stdout=subprocess.PIPE, shell=True)
  output, err = p.communicate()
  if len(output) == 0:
    print("PIP required", file=sys.stderr)
    sys.exit(1)
  global pip_cmd
  pip_cmd = output[:-1]

  # check gnu make
  p = subprocess.Popen('which make', stdout=subprocess.PIPE, shell=True)
  output, err = p.communicate()
  if len(output) == 0:
    print("Gnu Make required", file=sys.stderr)
    sys.exit(1)
  global make_cmd
  make_cmd = output[:-1]

  # check cmake
  p = subprocess.Popen('which cmake', stdout=subprocess.PIPE, shell=True)
  output, err = p.communicate()
  if len(output) == 0:
    print("CMake required", file=sys.stderr)
    sys.exit(1)
  global cmake_cmd
  cmake_cmd = output[:-1]

def verify_requirements_optional():
  # if rept1l or recola should be retrieved from repo then git is required
  if rept1l_git or recola_git:
    p = subprocess.Popen('which git', stdout=subprocess.PIPE, shell=True)
    output, err = p.communicate()
    if len(output) == 0:
      print("\n Git required if rept1l or recola should be cloned.",
            file=sys.stderr)
      sys.exit(1)
    global git_cmd
    git_cmd = output[:-1]

  # if rept1l or recola should be retrieved from repo then git is required
  p = subprocess.Popen('which form', stdout=subprocess.PIPE, shell=True)
  output, err = p.communicate()

  formcmd = None
  global build_form
  if len(output) <= 1 and not build_form:
    print("\n %(form)s is required, but not found in path. " % coloredsymbols)
    frm = ('\n Do you want to build %(form)s and let the system set'
           % coloredsymbols + ' the path to form?')
    if query_yes_no(frm):
      build_form = True
    else:
      print("\n %(form)s is essential for %(rept1l)s's functionality."
            % coloredsymbols)
      yfrmpath = (" Do you want to set path to the %(form)s exectuable now?"
                  % coloredsymbols)
      if query_yes_no(yfrmpath):
        formcmd = query_file('%(form)s executable' % coloredsymbols)

  elif build_form and len(output) > 1:
    print('\n The system discovered %(form)s in: ' % coloredsymbols + output)
    frm = (' Do you want to build %(form)s anyway? ' % coloredsymbols +
           'The %(form)s path will be set to the built version.'
           % coloredsymbols)
    if query_yes_no(frm):
      build_form = True
    else:
      build_form = False

  return formcmd

def run_command(command):
  """ Runs a (non-shell) command and prints its output. """
  process = subprocess.Popen(command, stdout=subprocess.PIPE)
  while True:
    output = process.stdout.readline()
    try:
       output = output.decode("utf-8")
    except:
      pass
    if output == '' and process.poll() is not None:
      break
    if output:
      print(output.strip())
  rc = process.poll()
  return rc

def install_tools(build_collier, build_form, get_recola, get_rept1l):
  """ Retrieves, builds and installs dependencies. """
  if any([build_collier, build_form]):

    if build_collier and not build_form:
      print('\n Building %(collier)s \n' % coloredsymbols)
    elif build_form and not build_collier:
      print('\n Building %(form)s \n' % coloredsymbols)
    else:
      print('\n Building %(collier)s and %(form)s \n' % coloredsymbols)

    # using the same path for installation and compilation
    rth_path = os.path.join(install_dir, 'rept1l_third-party')
    th_path = os.path.join(install_dir, 'rept1l_third-party')
    if not os.path.exists(rth_path):
      os.mkdir(rth_path)
    os.chdir(th_path)

    # force cmake colors
    os.environ['CLICOLOR_FORCE'] = '1'
    cmd = [cmake_cmd, '.', '-DCMAKE_INSTALL_PREFIX=' + rth_path]
    if build_collier:
      cmd = cmd + ["-DBUILD_COLLIER=On"]
    if build_form:
      cmd = cmd + ["-DBUILD_FORM=On"]
    run_command(cmd)

    cmd = [make_cmd]
    run_command(cmd)

    cmd = [make_cmd, 'install']
    run_command(cmd)

    cmd = [make_cmd, 'clean-all']
    run_command(cmd)

  if get_recola:
    print('\n Getting %(recola)s \n' % coloredsymbols)
    os.chdir(install_dir)
    if recola_git:
      cmd = [git_cmd, 'clone', recola_git_url]
      run_command(cmd)
    else:
      recurl = ('https://bitbucket.org/JNicL/recola-for-generic-models/get/' +
                'a36fc481c321.tar.gz')
      recsrc, _ = urlretrieve(recurl)
      tf = tarfile.open(recsrc)
      tf.extractall()
      try:
        os.rename('JNicL-recola-for-generic-models-a36fc481c321', 'recola')
      except OSError as e:
        print(e)

  if get_rept1l:
    print('\n Getting %(rept1l)s \n' % coloredsymbols)
    os.chdir(install_dir)
    if rept1l_git:
      cmd = [git_cmd, 'clone', rept1l_git_url]
      run_command(cmd)
    else:
      recurl = 'https://bitbucket.org/JNicL/rept1l/get/86ec9ea66685.tar.gz'
      recsrc, _ = urlretrieve(recurl)
      tf = tarfile.open(recsrc)
      tf.extractall()
      try:
        os.rename('JNicL-rept1l-86ec9ea66685', 'rept1l')
      except OSError as e:
        print(e)

  if rept1l_pip_requirements:
    print('\n Getting %(rept1l)s pip requirements\n' % coloredsymbols)
    os.chdir(install_dir)
    cmd = [pip_cmd, 'install', '--user', '-r',
           'rept1l_package_dependencies.txt']
    run_command(cmd)

#------------------------------------------------------------------------------#

def generate_sh(config_dir, install_dirs):
  """ Generate the sh that needs to be sourced """
  exp = 'export '
  sr = 'source '
  name = 'REPT1L'
  sources_list = ["# rept1l paths", ""]

  exp1 = exp + name + '_CONFIG=\"%s\"' % config_dir
  exp2 = exp + name + '_PATH=\"%s\"' % install_dirs['REPT1L']
  exp22 = exp + 'PYTHONPATH=$PYTHONPATH:"$(dirname "$REPT1L_PATH")"'
  exp23 = exp + 'PYTHONPATH=$PYTHONPATH:$REPT1L_CONFIG'

  sources_list += [exp1, exp2, exp22, exp23, ""]

  name = 'RECOLA_ROOT'
  exp31 = name + '_PATH=\"%s\"' % install_dirs[name]
  name = 'RECOLA'
  exp32 = exp + name + '_PATH=\"%s\"' % install_dirs[name]
  name = 'RECOLA_SOURCE'
  exp33 = exp + name + '_PATH=\"%s\"' % install_dirs[name]
  name = 'RECOLA_INCLUDE'
  exp37 = exp + name + '_PATH=\"%s\"' % install_dirs[name]
  name = 'COLLIER'
  exp34 = exp + name + '_PATH=\"%s\"/lib' % install_dirs[name]
  name = 'COLLIER_LIBRARY'
  exp35 = exp + name + '_DIR=${COLLIER_PATH}'
  name = 'COLLIER_INCLUDE'
  exp36 = exp + name + '_DIR=${COLLIER_PATH}/../lib'

  sources_list += [exp31, exp32, exp33, exp37, "", exp34, exp35, exp36, ""]

  name = 'PYRECOLA'
  if name in install_dirs:
    exp41 = exp + name + '_PATH=\"%s\"' % install_dirs[name]
  else:
    exp41 = exp + name + '_PATH=${RECOLA_ROOT_PATH}/pyrecola'
  exp5 = exp + 'PYTHONPATH=$PYTHONPATH:$' + name + '_PATH'

  name = 'PYRECOLA_SOURCE'
  exp42 = exp + name + '_PATH=\"%s\"' % install_dirs[name]

  sources_list += [exp41, exp42, exp5, ""]

  name = 'FORMPROC'
  if name in install_dirs:
    exp6 = name + '_PATH=\"%s\"' % install_dirs['FORMPROC']
  else:
    exp6 = name + '_PATH=${REPT1L_PATH}/formutils'
  exp7 = exp + 'FORMPATH=$FORMPATH:$' + name + '_PATH'

  sources_list += [exp6, exp7]

  name = 'REPT1L_TOOLS'
  if name in install_dirs:
    exp8 = name + '_PATH=\"%s\"' % install_dirs['REPT1L_TOOLS']
    exp9 = exp + 'PATH=$PATH:$' + name + '_PATH'

    sources_list += [exp8, exp9]

  return '\n'.join(sources_list)

#------------------------------------------------------------------------------#

def check_path(path):
  if path[-1] != '/':
    path += '/'

  path = os.path.expanduser(path)
  if not os.path.exists(path):
    answer = query_yes_no('Path does not exist, ' +
                          'do you want to create that path?')
    if answer is True:
      os.makedirs(path)
    else:
      print("Terminating installation.")
      end()

  return path

#------------------------------------------------------------------------------#

def query_path(name, **kwargs):
  print(' Please provide a path for `' + name + '`.')

  def complete(text, state):
    return (glob.glob(os.path.expanduser(text)+'*')+[None])[state]

  readline.set_completer_delims(' \t\n;')
  readline.parse_and_bind("tab: complete")
  readline.set_completer(complete)
  path = input()
  path = check_path(path, **kwargs)
  return path

def check_file(path):
  path = os.path.expanduser(path)
  return path, os.path.isfile(path)

def query_file(name, **kwargs):
  print(' Please provide a filepath for `' + name + '`.')

  def complete(text, state):
    return (glob.glob(os.path.expanduser(text)+'*')+[None])[state]

  readline.set_completer_delims(' \t\n;')
  readline.parse_and_bind("tab: complete")
  readline.set_completer(complete)
  filepath = input()
  filepath, exists = check_file(filepath, **kwargs)
  if not exists:
    print("Your filepath `" + filepath + "` does not exist.")
    return query_file(name, **kwargs)
  return filepath

#------------------------------------------------------------------------------#

def query_paths(install_mode):
  """ Setting up all environment variables. """
  paths = {}

  # all paths are fixed relative to the installation path
  if install_mode == 'auto_paths':
    paths['RECOLA_ROOT'] = os.path.join(install_dir, 'recola2.0')
    paths['RECOLA'] = '${RECOLA_ROOT_PATH}'
    paths['RECOLA_SOURCE'] = '${RECOLA_ROOT_PATH}/src'
    paths['RECOLA_INCLUDE'] = '${RECOLA_ROOT_PATH}/include'
    paths['PYRECOLA'] = '${RECOLA_ROOT_PATH}'
    paths['PYRECOLA_SOURCE'] = '${RECOLA_SOURCE_PATH}/pyrecola'
    paths['COLLIER'] = os.path.join(install_dir, 'rept1l_third-party')

  # custom installation, for ``advanced`` users
  elif install_mode == 'config_paths':
    print("\n REPT1L has run-time dependencies to Recola and %(form)s."
          % coloredsymbols)

    recola = query_path('Recola Root PATH')
    paths['RECOLA_ROOT'] = recola

    pyr = (' Do you want to set the Recola Library different from ' +
           'path `${RECOLA_ROOT_PATH}`?')

    if query_yes_no(pyr):
      recolaroot = query_path('Recola Library PATH')
      paths['RECOLA'] = recolaroot
    else:
      paths['RECOLA'] = '${RECOLA_ROOT_PATH}/recola'

    collier = query_path('Collier Library PATH')
    paths['COLLIER'] = collier

    pyr = (' Do you want to set the PyRecola path different from ' +
           'path `${RECOLA_ROOT_PATH}/pyrecola`?')
    if query_yes_no(pyr):
      pyrecola = query_path('PyRecola PATH')
      paths['PYRECOLA'] = pyrecola

    paths['RECOLA_SOURCE'] = '${RECOLA_ROOT_PATH}/recola'
    paths['PYRECOLA_SOURCE'] = '${RECOLA_ROOT_PATH}/pyrecola'

    frm = (' Do you want to set the %(form)s procedures path different from '
           % coloredsymbols + 'path `${REPT1L_PATH}/form`?')

    if query_yes_no(frm):
      formprc = query_path('%(form)s procedures PATH' % coloredsymbols)
      paths['FORMPROC'] = formprc

    tol = (' Do you want to export %(rept1l)s tools to PATH?' % coloredsymbols)

    if query_yes_no(tol):
      tolp = ('Do you want to export %(rept1l)s tools path different from ' +
              'path `${REPT1L_PATH}/tools`?' % coloredsymbols)
      if query_yes_no(tol):
        tools = query_path('%(rept1l)s tools PATH' % coloredsymbols)
      else:
        tools = '${REPT1L_PATH}/tools'
      paths['REPT1L_TOOLS'] = tools

  # the true custom installation, for ``advanced`` users
  elif install_mode == 'no_paths':
    paths['RECOLA_ROOT'] = ''
    paths['RECOLA'] = ''
    paths['RECOLA_SOURCE'] = ''
    paths['PYRECOLA'] = ''
    paths['PYRECOLA_SOURCE'] = ''
    paths['COLLIER'] = ''

  return paths

#------------------------------------------------------------------------------#

def show_post_installation_message(config_dir_rel):
  """ Print post installation information. """
  install_msg = "\n %(rept1l)s installed complete" % coloredsymbols

  sourced_file = os.path.expanduser('~/%s/rept1l.sh' % config_dir_rel)
  source_msg = "[[ -s \"%s\" ]] && source \"%s\"" % (sourced_file, sourced_file)

  if platform.system() == 'Darwin' and get_shell() == 'bash':
    rcfile = '.bash_profile'
  else:
    rcfile = '.%src' % get_shell()

  # check if rept1l is already sourced
  rcpath = os.path.expanduser('~/%s' % rcfile)

  def find_source(source_msg):
    try:
      with open(rcpath, 'r') as f:
        for line in f:
          yield source_msg in line.strip()
    except IOError as e:
      print('Could not rcread file: ' + rcpath)
      yield [False]

  found_source = any(find_source(sourced_file))
  if not found_source:
    print('\n The following line needs to be added to your ~/%s:' % rcfile)
    print('\n ' + source_msg + '\n')
    answer = query_yes_no('Do you want the skript to add it?')
    if answer:
      subprocess.Popen('echo "\n" >> ~/%s' % rcfile, shell=True)
      subprocess.Popen('echo "# REPTIL CONFIGS" >> ~/%s' % rcfile, shell=True)
      subprocess.Popen('echo "' + source_msg + '" >> ~/%s' % rcfile, shell=True)
  else:
    print(('Found source command in your ~/%s' % rcfile) +
          '. No need to add the line to your ~/%s' % rcfile)

  print("\n Please restart/source your shell.")

#------------------------------------------------------------------------------#

def build_default_config(rept1l_config_file, formcmd=None):
  """ (Over-)Writing the default rept1l config. """

  with open(rept1l_config_file, 'w') as f:
    with open(os.path.join(install_dir, 'config/rept1l_config.py'), 'r') as g:
      f.writelines(g.readlines())
    if formcmd:
      f.writelines(["\n", "# by default form executable from PATH\n",
                    "formcmd = '" + formcmd + "'"])
    else:
      f.writelines(["", "# by default form executable from PATH",
                    "#formcmd = 'path/to/form'"])

#------------------------------------------------------------------------------#

def end_installation():
  print("\n" + end)
  sys.exit()

#------------------------------------------------------------------------------#

def main():
  verify_requirements_essential()

  install_mode = {1: 'auto_paths', 2: 'config_paths', 3: 'no_paths', 4: 'quit'}

  im = install_mode[query_case(se, 4)]

  if im == 'quit':
    end_installation()

  if im == 'auto_paths':
    formcmd = verify_requirements_optional()
    install_tools(build_collier, build_form, get_recola, get_rept1l)
  else:
    formcmd = None

  install_dirs = query_paths(im)
  if im == 'auto_paths':
    install_dirs['REPT1L'] = os.path.join(install_dir, 'rept1l')
  else:
    install_dirs['REPT1L'] = install_dir

  if not os.path.exists(config_dir_abosulte_path):
    os.mkdir(config_dir_abosulte_path)

  # generate the sh file which is sourced, setting all necessary paths
  rept1l_sh_file = os.path.join(config_dir_abosulte_path, 'rept1l.sh')
  with open(rept1l_sh_file, 'w+') as f:
    f.write(generate_sh(config_dir_abosulte_path, install_dirs))

  # generate the python config file which is sourced during run-time
  rept1l_config_file = os.path.join(config_dir_abosulte_path,
                                    'rept1l_config.py')

  # when form is built, the path to the form executable used in python is set
  # explicitly
  if im == 'auto_paths' and build_form:
    formcmd = os.path.join(install_dir, 'rept1l_third-party/bin/form')

  build_default_config(rept1l_config_file, formcmd=formcmd)

  show_post_installation_message(config_dir_relative_path)

  end_installation()

#------------------------------------------------------------------------------#

if __name__ == "__main__":
  main()
